/**
 *
 * NAME		modal.js
 * DATE		2016-02-25
 * AUTHOR	Fabio Marciano
 * USAGE
 *          Modal({content:'<div id="loading"><div><h3>Por favor aguarde</h3></div></div>', width:320, height:100, close:null});
 *          Modal({content:'<a class="btn-close">Fechar</a>', width:500, height:200, close:['.btn-close']});
 *
**/

function Modal() {
	var args = arguments[0] || null;
	var data = {
		content:'',
		width:640,
		height:480,
		left:null,
		top:null,
		close:['.btn-close'],
		center:[true, true]
	};

	$('#modal').remove();

	if (args) {
		for (var i in args) {
			data[i] = args[i];
		}
	}

	data.center[0] &= data.left ? false : true;
	data.center[1] &= data.top ? false : true;

	data.root = $('<div id="modal"><div></div></div>');

	data.root.find('DIV').first().css('width', data.width).css('height', data.height).css('margin-left', (data.left != null ? data.left : parseInt(data.width / 2) * -1)).css('margin-top', (data.top != null ? data.top : parseInt(data.height / 2) * -1)).css('left', data.center[0] ? '50%' : '').css('top', data.center[1] ? '50%' : '');

	$('BODY').append(data.root);

	$('#modal DIV').first().html(data.content);

	if (data.close) {
		$('#modal').find(data.close.join(', ')).each(function() {
			$(this).click(function() {
				$('#modal').fadeOut(300, function() {
					$('#modal').remove();
					$('BODY').css('overflow', 'auto');
				});
			})
		});
	}

	$('#modal').fadeIn('fast', function() {
		$('BODY').css('overflow', 'hidden');
	});

	void(0);
};

$(document).ready(function() {
	$('#action-cross-header BUTTON').click(function(event) {
		event.preventDefault();
		$.ajax({
			url:'assets/include/form.html',
			beforeSend:function() {
				Modal({content:'<h6 class="loading">Por favor aguarde</h6>', width:320, height:120});
			},
			success:function(data) {
				Modal({content:data, width:720, height:215});
			}
		});
	});
});